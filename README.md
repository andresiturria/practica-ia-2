# Práctica 2   A*

![CC](/Imagenes/Licencia CC.png)


El programa encontrado en /src/aplicacion/Main.java imprime un camino utilizando la clase A*.

# Pregunta 1

¿Qué variable representa la lista ABIERTA?

La lista abierta que contiene los nodos que faltan por expandirse es representada por la variable openSet.

![OpenSet](/Imagenes/OpenSet.png)

# Pregunta 2

¿Qué variable representa la función g?

La función g, que calcula el coste del mejor camino desde el nodo inicial hasta el actual, es representada por la variable gScore.

![gScore](/Imagenes/gScore.png)

# Pregunta 3

¿Qué variable representa la función f?

La función f(n) = g(n) + h(n), es decir, la suma de g más la estimación positiva desde el nodo actual hasta el objetivo, es representada
por fScore.

![fScore](/Imagenes/fScore.png)

# Pregunta 4

¿Qué método habría que modificar para que la heurística representara la distancia aérea entre vértices?

El método heuristicCostEstimate por default devuelve 1, habría que cambiar este método para que representara la distancia aérea entre vértices.

![heuristicCostEstimate](/Imagenes/heuristicCostEstimate.png)

# Pregunta 5

¿Realiza este método reevaluación de nudos cuando se encuentra una nueva ruta a un determinado vértice? Justifique la respuesta.

![comparator](/Imagenes/comparator.png)

Cuando el algoritmo llega a un nudo de la lista abierta por un camino mejor se reevalua, pero no se propaga a los nudos ya expandidos, por lo cual h es consistente.